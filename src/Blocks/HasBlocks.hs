{-# LANGUAGE DefaultSignatures     #-}
{-# LANGUAGE QuantifiedConstraints #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE UndecidableInstances  #-}

module Blocks.HasBlocks where

import Blocks.Coordinate             (Derivative (..), TaylorCoeff (..))
import Bootstrap.Build.HasForce      (HasForce, forceM)
import Bootstrap.Math.DampedRational (DampedRational)
import Bootstrap.Math.DampedRational qualified as DR
import Bootstrap.Math.FreeVect       (FreeVect, evalFreeVect)
import Bootstrap.Math.Util           (factorial)
import Bootstrap.Math.VectorSpace    (Tensor, VectorSpace (..), (^/))
import Control.DeepSeq               (NFData)
import Control.Monad.Zip             (mzipWith)
import Data.Foldable                 qualified as Foldable
import Data.Functor.Compose          (Compose (..))
import Data.Functor.Identity         (Identity (..))
import Data.Kind                     (Type)
import Data.Matrix.Static            (Matrix)
import Data.Reflection               (Reifies, reflect, reify)
import Data.Tagged                   (Tagged, proxy, unproxy)
import Data.Vector                   (Vector)
import GHC.Exts                      (Constraint)
import GHC.Generics                  (Generic, K1 (..), M1 (..), Rep, from,
                                      (:+:) (..))
import Linear.V                      (Dim, V)
import Linear.V                      qualified as L

type HasBlocks b p m a = ( BlockFetchContext b a m
                         , Reifies p (BlockTableParams b)
                         , Applicative m
                         , HasForce m
                         )

type CrossingMat j n b = Matrix j j `Tensor` V n `Tensor` FreeVect b

type family BlockFetchContext b a (m :: Type -> Type) :: Constraint

type family BlockTableParams b :: Type

type family BlockBase b a :: Type

class (RealFloat a, Eq a, NFData a) => IsolatedBlock b deriv a where
  getBlockIsolated
    :: (HasBlocks b p m a, Applicative v, Foldable v)
    => v deriv
    -> b
    -> Compose (Tagged p) m (v a)

  default getBlockIsolated
    :: ( Generic b
       , GIsolatedBlock (Rep b) deriv a
       , BlockTableParams b ~ BlockTableParamsGeneric b
       , BlockFetchContext b a m ~ BlockFetchContextGeneric b a m
       , HasBlocks b p m a
       , Applicative v
       , Foldable v
       )
    => v deriv
    -> b
    -> Compose (Tagged p) m (v a)
  getBlockIsolated deriv b = genericGetBlockIsolated deriv (from @b @() b)

class IsolatedBlock b deriv a => ContinuumBlock b deriv a where
  getBlockContinuum
    :: (HasBlocks b p m a, Applicative v, Foldable v)
    => v deriv
    -> b
    -> Compose (Tagged p) m (DR.DampedRational (BlockBase b a) v a)

  default getBlockContinuum
    :: ( Generic b
       , GContinuumBlock (Rep b) deriv a
       , BlockTableParams b ~ BlockTableParamsGeneric b
       , BlockFetchContext b a m ~ BlockFetchContextGeneric b a m
       , BlockBase b a ~ BlockBaseGeneric b a
       , HasBlocks b p m a
       , Applicative v
       , Foldable v
       )
    => v deriv
    -> b
    -> Compose (Tagged p) m (DR.DampedRational (BlockBase b a) v a)
  getBlockContinuum deriv b = genericGetBlockContinuum deriv (from @b @() b)

--------------- Generics ---------------
-- | This Generics machinery allows us to derive IsolatedBlock and
-- ContinuumBlock automatically for sum types. For example, we can
-- write
--
--   data MyBlock = SB ScalarBlock | B3d Block3d
--     deriving (Generic)
--
--   type instance BlockFetchContext MyBlock a m = BlockFetchContextGeneric MyBlock a m
--   type instance BlockBase         MyBlock a   = BlockBaseGeneric         MyBlock a
--   type instance BlockTableParams  MyBlock     = BlockTableParamsGeneric  MyBlock
--   instance KnownCoordinate c => IsolatedBlock  MyBlock (Derivative c) a
--   instance KnownCoordinate c => ContinuumBlock MyBlock (Derivative c) a
--
-- And the compiler will derive the required instances
-- automatically. Note that we do not provide generic instances for
-- product types. A tutorial on writing Generics-based code is here:
-- https://wiki.haskell.org/GHC.Generics

-- First we define BlockFetchContext, BlockTableParams, BlockBase for
-- 'Rep's. Note that 'Rep b x' takes an additional type parameter 'x',
-- which apparently does nothing. We specialize 'x' to () everywhere
-- below.

type instance BlockFetchContext (M1 i c f ())  a m = BlockFetchContext (f ()) a m
type instance BlockFetchContext (K1 i f ())    a m = BlockFetchContext f a m
type instance BlockFetchContext ((f :+: g) ()) a m = (BlockFetchContext (f ()) a m, BlockFetchContext (g ()) a m)

type instance BlockTableParams (M1 i c f ())  = BlockTableParams (f ())
type instance BlockTableParams (K1 i f ())    = BlockTableParams f
type instance BlockTableParams ((f :+: g) ()) = (BlockTableParams (f ()), BlockTableParams (g ()))

type instance BlockBase (M1 i c f ())  a = BlockBase (f ()) a
type instance BlockBase (K1 i f ())    a = BlockBase f a
type instance BlockBase ((f :+: g) ()) a = BlockBase (f ()) a

-- | Type families that can be used as the default for defining
-- BlockFetchContext etc. for sum types.
type family BlockTableParamsGeneric b where
  BlockTableParamsGeneric b = BlockTableParams (Rep b ())

type family BlockFetchContextGeneric b a m where
  BlockFetchContextGeneric b a m = BlockFetchContext (Rep b ()) a m

type family BlockBaseGeneric b a where
  BlockBaseGeneric b a = BlockBase (Rep b ()) a

-- | A generic version of IsolatedBlock
class GIsolatedBlock f deriv a where
  genericGetBlockIsolated
    :: (HasBlocks (f ()) p m a, Applicative v, Foldable v)
    => v deriv
    -> f ()
    -> Compose (Tagged p) m (v a)

instance GIsolatedBlock f deriv a => GIsolatedBlock (M1 i c f) deriv a where
  genericGetBlockIsolated deriv (M1 b) = genericGetBlockIsolated deriv b

instance IsolatedBlock b deriv a => GIsolatedBlock (K1 i b) deriv a where
  genericGetBlockIsolated deriv (K1 b) = getBlockIsolated deriv b

-- Really need to define a library module for this
runTagged :: forall a r. a -> (forall (s :: Type) . Reifies s a => Tagged s r) -> r
runTagged a f = reify a (proxy f)

instance (GIsolatedBlock f deriv a, GIsolatedBlock g deriv a) => GIsolatedBlock (f :+: g) deriv a where
  genericGetBlockIsolated dv b = Compose $ unproxy $ \p ->
    let (p1,p2) = reflect p
    in case b of
      L1 b1 -> runTagged p1 $ getCompose $ genericGetBlockIsolated dv b1
      R1 b2 -> runTagged p2 $ getCompose $ genericGetBlockIsolated dv b2

class GContinuumBlock f deriv a where
  genericGetBlockContinuum
    :: (HasBlocks (f ()) p m a, Applicative v, Foldable v)
    => v deriv
    -> f ()
    -> Compose (Tagged p) m (DR.DampedRational (BlockBase (f ()) a) v a)

instance GContinuumBlock f deriv a => GContinuumBlock (M1 i c f) deriv a where
  genericGetBlockContinuum deriv (M1 b) = genericGetBlockContinuum deriv b

instance ContinuumBlock b deriv a => GContinuumBlock (K1 i b) deriv a where
  genericGetBlockContinuum deriv (K1 b) = getBlockContinuum deriv b

instance ( GContinuumBlock f deriv a
         , GContinuumBlock g deriv a
         , BlockBase (g ()) a ~ BlockBase (f ()) a
         ) => GContinuumBlock (f :+: g) deriv a where
  genericGetBlockContinuum dv b = Compose $ unproxy $ \p ->
    let (p1,p2) = reflect p
    in case b of
      L1 b1 -> runTagged p1 $ getCompose $ genericGetBlockContinuum dv b1
      R1 b2 -> runTagged p2 $ getCompose $ genericGetBlockContinuum dv b2

--------------- Instances for Either ---------------

-- | The instances for Either are examples of Generics-based deriving
-- for a sum type

type instance BlockFetchContext (Either b1 b2) a m = BlockFetchContextGeneric (Either b1 b2) a m
type instance BlockTableParams  (Either b1 b2)     = BlockTableParamsGeneric  (Either b1 b2)
type instance BlockBase (Either b1 b2) a = BlockBaseGeneric (Either b1 b2) a

-- | The following general instances overlap. That is,
-- a block of type IsolatedBlock (Either b1 b2) (TaylorCoeff deriv) a
-- has an ambiguous definition of getBlockIsolated. However, we know that
-- these instances actually commute with each other. We tell GHC this
-- by giving the pragma INCOHERENT. In this circumstance, the compiler
-- knows not to worry about there being multiple instances, and just picks
-- an arbitrary one. An alternative approach is to declare the Either
-- instance OVERLAPPING the OVERLAPPABLE TaylorCoeff instance or vice versa.
-- OVERLAPPING and OVERLAPPABLE together are equivalent to OVERLAPS. However
-- GHC would then come back complaining about the instances being ambiguous
-- again. So the most 'neutral' choice is to just say it's INCOHERENT.
instance {-# INCOHERENT #-}
  (IsolatedBlock b1 deriv a, IsolatedBlock b2 deriv a) =>
  IsolatedBlock (Either b1 b2) deriv a
instance {-# INCOHERENT #-}
  (ContinuumBlock b1 deriv a, ContinuumBlock b2 deriv a, BlockBase b1 a ~ BlockBase b2 a) =>
  ContinuumBlock (Either b1 b2) deriv a

--------------- Taylor coefficients ---------------

instance {-# INCOHERENT #-}
  (IsolatedBlock b deriv a, Eq a, RealFloat a, NFData a, deriv ~ Derivative c) =>
  IsolatedBlock b (TaylorCoeff deriv) a where
  getBlockIsolated derivV b =
    fmap (liftA2 taylorRescale derivV) $
    getBlockIsolated (fmap unTaylorCoeff derivV) b
    where
      taylorRescale t = runIdentity . taylorRescaleV t . Identity

instance {-# INCOHERENT #-}
  (ContinuumBlock b deriv a, deriv ~ Derivative c) =>
  ContinuumBlock b (TaylorCoeff deriv) a where
  getBlockContinuum derivV b =
    fmap (DR.mapNumerator (liftA2 taylorRescaleV derivV)) $
    getBlockContinuum (fmap unTaylorCoeff derivV) b

taylorRescaleV
  :: (VectorSpace v, IsBaseField v a, Fractional a) => TaylorCoeff (Derivative c) -> v a -> v a
taylorRescaleV (TaylorCoeff (Derivative (m,n))) v =
  v ^/ (fromIntegral (factorial m) * fromIntegral (factorial n))

--------------- Getting CrossingMat's ---------------

-- | A newtype wrapper around m (v a) that provides a 'VectorSpace'
-- instance where the data is deeply evaluated using 'forceM' after
-- every 'add'.
--
-- This allows us to perform deep evaluation between monadic
-- actions. For example, if we read several 'v a's from files in the
-- 'm' monad, wrapping them in 'ForceVecM m v a' will ensure that we
-- interleave arithmetic with fetching from files, so that we do not
-- build up large un-parsed files in memory before performing
-- arithmetic.
--
-- Why do we forceM only after 'add' and not 'scale'? Well, 'add'
-- combines two vectors, so by forcing the result we have the
-- potential to reduce memory usage by combining the data from one
-- vector into another. Meanwhile 'scale' simply builds up a thunk
-- containing a single vector and a constant. We assume the constant
-- doesn't take up much memory, so it is not necessary to force the
-- result of 'scale'. Furthermore, by not forcing 'scale', we can
-- potentially avoid traversing the vector more than necessary.
--
newtype ForceMVec m v a = MkForceMVec (Compose m v a)

instance ( forall a . NFData a => NFData (v a)
         , VectorSpace v
         , Applicative m
         , HasForce m
         ) => VectorSpace (ForceMVec m v) where
  type IsBaseField (ForceMVec m v) a = (IsBaseField v a, NFData a)
  zero = MkForceMVec zero
  scale a (MkForceMVec x) = MkForceMVec $ scale a x
  add (MkForceMVec v1) (MkForceMVec v2) = MkForceMVec $ forceM (add v1 v2)

runForceMVec :: ForceMVec m v a -> m (v a)
runForceMVec (MkForceMVec (Compose mv)) = mv

-- | Fetch an isolated block and forceM the result
getBlockIsolatedForceMVec
  :: ( forall x . NFData x => NFData (v x)
     , HasBlocks b p m a
     , IsolatedBlock b deriv a
     , Applicative v
     , Foldable v
     )
  => v deriv
  -> b
  -> ForceMVec (Compose (Tagged p) m) v a
getBlockIsolatedForceMVec dv b = MkForceMVec (Compose (forceM $ getBlockIsolated dv b))

-- | Fetch a continuum block and forceM the result
getBlockContinuumForceMVec
  :: ( forall x . NFData x => NFData (v x)
     , HasBlocks b p m a
     , ContinuumBlock b deriv a
     , Applicative v
     , Foldable v
     )
  => v deriv
  -> b
  -> ForceMVec (Compose (Tagged p) m) (DR.DampedRational (BlockBase b a) v) a
getBlockContinuumForceMVec dv b = MkForceMVec (Compose (forceM $ getBlockContinuum dv b))

-- | If b has a ContinuumBlock instance, we can compute an isolated
-- block by evaluating the DampedRational at 0. It may sometimes be
-- possible to write more efficient versions of getBlockIsolated' by
-- hand.
getBlockIsolatedDefault
  :: (ContinuumBlock b deriv a, Reifies (BlockBase b a) a, HasBlocks b p m a, Applicative v, Foldable v)
  => v deriv
  -> b
  -> Compose (Tagged p) m (v a)
getBlockIsolatedDefault dv b = DR.evalCancelPoleZeros 0 <$> getBlockContinuum dv b

getBlockFreeVectIsolated
  :: (HasBlocks b p m a, IsolatedBlock b deriv a)
  => Vector deriv
  -> FreeVect b a
  -> Compose (Tagged p) m (Vector a)
getBlockFreeVectIsolated derivsV v =
  L.reifyVector derivsV $ \derivs ->
  fmap L.toVector $
  runForceMVec $
  evalFreeVect (getBlockIsolatedForceMVec derivs) v

getBlockFreeVectContinuum
  :: (HasBlocks b p m a, ContinuumBlock b deriv a)
  => Vector deriv
  -> FreeVect b a
  -> Compose (Tagged p) m (DampedRational (BlockBase b a) Vector a)
getBlockFreeVectContinuum derivsV v =
  L.reifyVector derivsV $ \derivs ->
  fmap (DR.mapNumerator L.toVector) $
  runForceMVec $
  evalFreeVect (getBlockContinuumForceMVec derivs) v

getCrossingMatIsolated
  :: (HasBlocks b p m a, Dim n, IsolatedBlock b deriv a)
  => V n (Vector deriv)
  -> CrossingMat j n b a
  -> Compose (Tagged p) m (Matrix j j (Vector a))
getCrossingMatIsolated derivsV =
  traverse (fmap Foldable.msum . getElem) . getCompose
  where
    getElem = sequenceA . mzipWith getBlockFreeVectIsolated derivsV . getCompose

getCrossingMatContinuum
  :: (HasBlocks b p m a, Dim n, ContinuumBlock b deriv a)
  => V n (Vector deriv)
  -> CrossingMat j n b a
  -> Compose (Tagged p) m (DampedRational (BlockBase b a) (Matrix j j `Tensor` Vector) a)
getCrossingMatContinuum derivsV =
  fmap DR.sequence .
  traverse (fmap concatElem . getElem) .
  getCompose
  where
    getElem = sequenceA . mzipWith getBlockFreeVectContinuum derivsV . getCompose
    concatElem = DR.mapNumerator (Foldable.msum . getCompose) . DR.sequence
