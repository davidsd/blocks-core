{-# LANGUAGE DataKinds          #-}
{-# LANGUAGE DeriveAnyClass     #-}
{-# LANGUAGE DerivingStrategies #-}

module Blocks.Coordinate where

import Blocks.Sign         (Sign (..))
import Blocks.Sign         qualified as Sign
import Bootstrap.Math.Util (choose, factorial)
import Control.DeepSeq     (NFData)
import Control.Exception   (Exception, throw)
import Data.Aeson          (FromJSON (..), ToJSON (..))
import Data.Binary         (Binary)
import Data.Coerce         (coerce)
import Data.Foldable       (toList)
import Data.Map.Strict     (Map)
import Data.Map.Strict     qualified as Map
import Data.Ratio          ((%))
import Data.Set            qualified as Set
import Data.Typeable       (Typeable, typeOf)
import Data.Vector         qualified as V
import GHC.Generics        (Generic)

-- | Coordinates understood by either ScalarBlocks or
-- Blocks3d. Currently, XT_Radial and WS_Radial are unsupported,
-- except in parsing Blocks3d tables.
data Coordinate = ZZb | XT | XT_Radial | YYb | WS | WS_Radial
  deriving (Eq, Ord, Enum, Bounded, Show, Generic, Binary, NFData)

-- | A pair of integers (m,n) tagged with a coordinate, representing
-- the derivative \partial_a^m \partia_b^n with coordinates a,b.
newtype Derivative c = Derivative { unDerivative :: (Int, Int) }
  deriving (Eq, Ord, Show, Generic)
  deriving anyclass (Binary, NFData)

-- | A Taylor series coefficient corresponding to the given
-- derivative. 'TaylorCoeff (Derivative (m,n))' represents
--
-- > (1/(m! n!)) \partial_a^m \partia_b^n.
newtype TaylorCoeff deriv = TaylorCoeff { unTaylorCoeff :: deriv }
  deriving (Eq, Ord, Show, Generic)
  deriving anyclass (Binary, NFData)

instance ToJSON Coordinate where
  toJSON = toJSON . coordinateToString

instance FromJSON Coordinate where
  parseJSON v = parseJSON v >>= coordinateFromString

coordinateToString :: Coordinate -> String
coordinateToString c = case c of
  ZZb       -> "zzb"
  XT        -> "xt"
  XT_Radial -> "xt_radial"
  YYb       -> "yyb"
  WS        -> "ws"
  WS_Radial -> "ws_radial"

-- | The inverse of a function with bounded argument type
inverseBounded :: (Bounded a, Enum a, Eq b) => (a -> b) -> b -> Maybe a
inverseBounded f = flip lookup inverseMap
  where
    inverseMap = zip (map f domain) domain
    domain = [minBound .. maxBound]

coordinateFromString :: MonadFail m => String -> m Coordinate
coordinateFromString s =
  maybe (fail ("Unknown coordinate: " ++ s)) pure $
  inverseBounded coordinateToString s

data CoordinateDir = XDir | YDir | CrossingDir

xEven, xOdd :: Sign 'XDir
xEven = Plus
xOdd  = Minus

yEven, yOdd :: Sign 'YDir
yEven = Plus
yOdd  = Minus

crossEven, crossOdd :: Sign 'CrossingDir
crossEven = Plus
crossOdd  = Minus

-- | A type-level constraint for a coordinate c.
class KnownCoordinate c where
  -- | Reflect 'c' into a value
  reflectCoordinate :: proxy c -> Coordinate

  -- | Convert a map of ZZb derivatives into a map of c derivatives,
  -- for the given Sign and nmax.
  fromZZb :: Floating a => Sign 'YDir -> Int -> Map (Derivative 'ZZb) a -> Map (Derivative c) a

  -- | Get the derivative from a map, possibly using Sign to complete
  -- the map. TODO: This is a bit clunky. Is there a more elegant way
  -- to do it?
  getDeriv :: Floating a => Sign 'YDir -> Map (Derivative c) a -> Derivative c -> a

  -- | Measure whether a given derivative is even under crossing
  -- (z,zb) -> (1-z,1-zb). Default instance in terms of crossingOdd.
  crossingEven :: Derivative c -> Bool
  crossingEven = not . crossingOdd

  -- | Measure whether a given derivative is even under crossing
  -- (z,zb) -> (1-z,1-zb). Default instance in terms of crossingEven.
  crossingOdd :: Derivative c -> Bool
  crossingOdd = not . crossingEven

data DerivMapException c = DerivMapException
  { exceptionDerivative :: Derivative c
  , exceptionMapKeys    :: [Derivative c]
  , exceptionMsg        :: String
  } deriving (Show, Exception)

derivMapErr :: (Typeable c, Typeable k) => Derivative (c :: k) -> Map (Derivative c) a -> e
derivMapErr d dMap = throw $
  DerivMapException d (Map.keys dMap) $
  "Derivative not among keys in a Derivative map of type " ++ show (typeOf d)

instance KnownCoordinate 'ZZb where
  reflectCoordinate _ = ZZb
  fromZZb _ _ = id
  getDeriv sign derivMap d@(Derivative (m,n))
    | m >= n    = Map.findWithDefault err d derivMap
    | otherwise =
      Sign.toNum sign * Map.findWithDefault err (Derivative (n,m)) derivMap
    where
      err = derivMapErr d derivMap
  crossingEven (Derivative (m,n)) = even (m+n)

instance KnownCoordinate 'XT where
  reflectCoordinate _ = XT
  fromZZb = zzbToXT
  getDeriv _ derivMap d =
    Map.findWithDefault (derivMapErr d derivMap) d derivMap
  crossingEven (Derivative (m,_)) = even m

instance KnownCoordinate 'YYb where
  reflectCoordinate _ = YYb
  fromZZb = zzbToYYb
  getDeriv sign derivMap d@(Derivative (m,n))
    | m >= n    = Map.findWithDefault err d derivMap
    | otherwise =
      Sign.toNum sign * Map.findWithDefault err (Derivative (n,m)) derivMap
    where
      err = derivMapErr d derivMap
  crossingEven (Derivative (m,n)) = even (m+n)

instance KnownCoordinate 'WS where
  reflectCoordinate _ = WS
  fromZZb sign nmax = yybToWS sign nmax . fromZZb sign nmax
  getDeriv _ derivMap d =
    Map.findWithDefault (derivMapErr d derivMap) d derivMap
  crossingEven (Derivative (m,_)) = even m

type Derivs c = Int -> V.Vector (Derivative c)

zzbDerivsAll :: Derivs 'ZZb
zzbDerivsAll nmax = V.fromList $ do
  m <- [0 .. lambda]
  n <- [0 .. min (lambda - m) m]
  pure $ Derivative (m,n)
  where
    lambda = 2*nmax-1

zzbDerivs :: Sign 'CrossingDir -> Derivs 'ZZb
zzbDerivs Plus  = V.filter crossingEven . zzbDerivsAll
zzbDerivs Minus = V.filter crossingOdd  . zzbDerivsAll

-- | Derivatives dx^m dt^n where m+2n <= lambdaPlusMinus, described in
-- https://gitlab.com/bootstrapcollaboration/blocks_3d/-/blob/master/doc/conventions.pdf
xtDerivsAll :: Sign 'YDir -> Derivs 'XT
xtDerivsAll ySign nmax = V.fromList $ do
  m <- [0 .. lambdaPlusMinus]
  n <- [0 .. (lambdaPlusMinus - m) `div` 2]
  pure $ Derivative (m,n)
  where
    lambda = 2*nmax - 1
    lambdaPlusMinus = case ySign of
      Plus  -> lambda
      Minus -> lambda - 1

-- | The Sign 'YDir only controls the number of derivatives, due to
-- the conventions in blocks_3d. The Sign 'XDir controls the parity of
-- derivatives in the x-direciton.
xtDerivs :: Sign 'XDir -> Sign 'YDir -> Derivs 'XT
xtDerivs xSign ySign = V.filter keepDeriv . xtDerivsAll ySign
  where
    keepDeriv (Derivative (m,_)) = case xSign of
      Plus  -> even m
      Minus -> odd m

xtDerivsRadial :: Sign 'XDir -> Derivs 'XT
xtDerivsRadial xSign = V.filter keepDeriv . xtDerivs xSign yEven
  where
    keepDeriv (Derivative (_,n)) = n == 0

xtDerivsConstant :: Derivs 'XT
xtDerivsConstant = V.filter keepDeriv . xtDerivsRadial xEven
  where
    keepDeriv (Derivative (m,n)) = m == 0 && n == 0

yybDerivsAll :: Derivs 'YYb
yybDerivsAll = coerce zzbDerivsAll

yybDerivs :: Sign 'XDir -> Derivs 'YYb
yybDerivs = coerce zzbDerivs

type Taylors c = Int -> V.Vector (TaylorCoeff (Derivative c))

zzbTaylorsAll :: Taylors 'ZZb
zzbTaylorsAll = fmap TaylorCoeff . zzbDerivsAll

zzbTaylors :: Sign 'CrossingDir -> Taylors 'ZZb
zzbTaylors x = fmap TaylorCoeff . zzbDerivs x

xtTaylorsAll :: Sign 'YDir -> Taylors 'XT
xtTaylorsAll t = fmap TaylorCoeff . xtDerivsAll t

xtTaylors :: Sign 'XDir -> Sign 'YDir -> Taylors 'XT
xtTaylors x t = fmap TaylorCoeff . xtDerivs x t

xtTaylorsRadial :: Sign 'XDir -> Taylors 'XT
xtTaylorsRadial x = fmap TaylorCoeff . xtDerivsRadial x

xtTaylorsConstant :: Taylors 'XT
xtTaylorsConstant = fmap TaylorCoeff . xtDerivsConstant

wsTaylorsAll :: Sign 'YDir -> Taylors 'WS
wsTaylorsAll = coerce xtTaylorsAll

wsTaylors :: Sign 'XDir -> Sign 'YDir -> Taylors 'WS
wsTaylors = coerce xtTaylors

wsTaylorsRadial :: Sign 'XDir -> Taylors 'WS
wsTaylorsRadial = coerce xtTaylorsRadial

wsTaylorsConstant :: Taylors 'WS
wsTaylorsConstant = coerce xtTaylorsConstant

-- | Assumes that the map for zzb derivatives has (m,n) with
-- m>=n. Should perhaps change to trying to lookup m>=n first and then
-- trying to lookup m<n if fails.
zzbToXT :: forall a. Floating a => Sign 'YDir -> Int -> Map (Derivative 'ZZb) a -> Map (Derivative 'XT) a
zzbToXT sign nmax derivs =
  Map.fromSet compute . Set.fromList . toList $ xtDerivsAll sign nmax
  where
    parity :: Int
    parity = case sign of { Plus -> 0; Minus -> 1 }

    compute :: Derivative 'XT -> a
    compute (Derivative (n,m')) = sum [
      let i = n+m-k-l
          j = k+l
      in
        (zzb i j) * fromRational ((-1)^l * factorial ((m-parity) `div` 2)
                                  * choose n k
                                  * choose m l
                                  % factorial m)
      | k <- [0..n], l <- [0..m] ]
      where m = 2*m'+parity

    zzb :: Int -> Int -> a
    zzb m n = if m >=n then
        Map.findWithDefault (e m n) (Derivative (m,n)) derivs
      else
        Sign.toNum sign * Map.findWithDefault (e n m) (Derivative (n,m)) derivs
    e m n = error $ "Derivative (" ++ show m ++ "," ++ show n ++ ") does not exist in the zzb map supplied to zzbToXT"

yybToWS :: forall a. Floating a => Sign 'YDir -> Int -> Map (Derivative 'YYb) a -> Map (Derivative 'WS) a
yybToWS sign nmax = Map.mapKeys coerce . zzbToXT sign nmax . Map.mapKeys coerce

zzbToYYb :: forall a. Floating a => Sign 'YDir -> Int -> Map (Derivative 'ZZb) a -> Map (Derivative 'YYb) a
zzbToYYb sign nmax derivs =
  Map.fromSet compute . Set.fromList . toList $ yybDerivsAll nmax
  where
    compute :: Derivative 'YYb -> a
    compute (Derivative (m,n)) = sum [
        let km = (m - m') `div` 2
            kn = (n - n') `div` 2
        in
          zzb m' n' * fromRational (((-1)^(kn+km) * choose' (m'+km-1) km * choose' (n'+kn-1) kn
                                     * factorial m * factorial n)
                                     % (factorial m' * factorial n'))
      | m' <- [m, m-2 .. 0], n' <- [n, n-2 .. 0]]
      where
        choose' :: Int -> Int -> Integer
        choose' _ 0 = 1
        choose' a b = choose a b
    zzb :: Int -> Int -> a
    zzb m n = if m >=n then
        Map.findWithDefault (e m n) (Derivative (m,n)) derivs
      else
        Sign.toNum sign * Map.findWithDefault (e n m) (Derivative (n,m)) derivs
    e m n = error $ "Derivative (" ++ show m ++ "," ++ show n ++ ") does not exist in the zzb map supplied to zzbToYYB"
