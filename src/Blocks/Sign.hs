{-# LANGUAGE DeriveAnyClass #-}

module Blocks.Sign where

import Control.DeepSeq (NFData)
import Data.Aeson      (FromJSON (..), ToJSON (..))
import Data.Binary     (Binary)
import GHC.Generics    (Generic)

-- | Sign carries a phantom type variable 's' that labels what the
-- sign is referring to, e.g. Sign 'XDir.
data Sign s = Plus | Minus
  deriving (Eq, Ord, Show, Generic, Binary, NFData)

signFromInt :: MonadFail m => Int -> m (Sign s)
signFromInt 1    = pure Plus
signFromInt (-1) = pure Minus
signFromInt s    = fail $ "Sign should be 1 or -1, not " ++ show s

toNum :: Num a => Sign s -> a
toNum Plus  = 1
toNum Minus = -1

fromNum :: (Num a, Ord a) => a -> Sign s
fromNum a | a >= 0    = Plus
          | otherwise = Minus

coerce :: Sign (s :: k) -> Sign (s' :: k')
coerce Plus  = Plus
coerce Minus = Minus

-- | The ToJSON and FromJSON instances should match the representation
-- of signs by blocks_3d.
instance ToJSON (Sign s) where
  toJSON = toJSON . toNum @Int

instance FromJSON (Sign s) where
  parseJSON v = parseJSON v >>= signFromInt

instance Semigroup (Sign s) where
  Plus  <> s     = s
  Minus <> Plus  = Minus
  Minus <> Minus = Plus

instance Monoid (Sign s) where
  mempty = Plus
