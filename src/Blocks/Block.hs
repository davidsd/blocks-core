{-# LANGUAGE DeriveAnyClass #-}

module Blocks.Block where

import Control.DeepSeq (NFData)
import Data.Aeson      (FromJSON, ToJSON)
import Data.Binary     (Binary)
import GHC.Generics    (Generic)

data DerivMultiplier
  = SChannel
  | TChannel
  deriving (Ord, Eq, Enum, Show, Generic, Binary, ToJSON, FromJSON, NFData)

data Block t f = Block
  { struct12         :: t
  , struct43         :: t
  , fourPtFunctional :: f
  } deriving (Eq, Ord, Show, Generic, Binary, ToJSON, FromJSON, NFData)

unzipBlock :: Block (t1, t2) (f1, f2) -> (Block t1 f1, Block t2 f2)
unzipBlock (Block (a1,a2) (b1,b2) (f1,f2)) =
  (Block a1 b1 f1, Block a2 b2 f2)

