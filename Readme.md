blocks-core
---------------

Core datatypes and functions for conformal blocks in bootstrap
computations. Other blocks libraries are built off of this one.

Documentation
-------------

[Documentation is here](https://davidsd.gitlab.io/blocks-core/)
